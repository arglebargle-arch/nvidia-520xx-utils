this is a (hopefully) short-term package to workaround the CTX timeout crash
that more recent drivers cause when playing proton games

see this thread for details:
https://forums.developer.nvidia.com/t/multiple-cuda-rtx-vulkan-application-crashing-with-xid-13-109-errors/235459/109

this package will very likely not be updated once the bug is fixed, you're
free to do what you want with it if it appears abandoned or stale
